# Password Guessing

This project is about password guessing. More specifically, you can define the complexity of the password and all the possible permutations are stored in a text file.

## Quick start
```
git clone https://gitlab.com/iliasmurikis/password-guessing.git
cd password_guessing/
python3 main.py
```
More complex query:
```
python3 main.py --length 6 --alphanumer y --symbols y
```

## Usage
To run script you can use the following arguments:
| Argument | Values | Meaning |
| :----   | :----: | :----  |
| --length | integer <em>(default: 4)</em> | How many characters in the password |
| --alphanumer| y/n <em>(default: n)</em> | Whether the password contains alphanumeric values |
| --capitals | y/n <em>(default: n)</em> | Whether the password contains capitals |
| --symbols | y/n <em>(default: n)</em> | Whether the password contains symbols |
