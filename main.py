import sys
import string
import argparse
import math
from itertools import product

""" Define scripts arguments """
parser = argparse.ArgumentParser(description='')
parser.add_argument('--length', type=int, nargs=1, default=[4],
                    help='sum the integers (default: find the max)')
parser.add_argument('--alphanumer', type=str, nargs=1, default=['n'],
                    help='calculate all alphanumerical cases(y/n)')
parser.add_argument('--capitals', type=str, nargs=1, default=['n'],
                    help='contain capitals(y/n)')
parser.add_argument('--symbols', type=str, nargs=1, default=['n'],
                    help='contain symbols(y/n)')
args = parser.parse_args()

items = list(range(1, 10))

""" Check length argument """
if args.length[0] < 0 and not isinstance(args.length[0], int):
    raise ValueError('Invalid value for "--length" argument. Please choose a positive integer.')
    sys.exit(1)

""" Check if should be alphanumerical """
if args.alphanumer[0].lower() == 'y':
    items += list(string.ascii_lowercase)
elif args.alphanumer[0].lower() == 'n':
    pass
else:
    raise ValueError('Invalid value for "--alphanumer" argument. Please choose (y/n).')
    sys.exit(1)

""" Check if should contain capital letters """
if args.capitals[0].lower() == 'y':
    items += list(string.ascii_uppercase)
elif args.capitals[0].lower() == 'n':
    pass
else:
    raise ValueError('Invalid value for "--capitals" argument. Please choose (y/n).')
    sys.exit(1)

""" Check if should contain symbols """
if args.symbols[0].lower() == 'y':
    items += list(string.punctuation)
elif args.symbols[0].lower() == 'n':
    pass
else:
    raise ValueError('Invalid value for "--symbols" argument. Please choose (y/n).')
    sys.exit(1)

combinations = [p for p in product(items, repeat=args.length[0])]

# print(len(combinations))
# print(len(items)**args.length[0])

with open('temp.txt', 'w+') as file:
    for combination in combinations:
        file.write('%s\n' % ''.join(str(x) for x in combination))
